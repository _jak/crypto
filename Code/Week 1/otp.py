import string
from operator import __and__


def strxor(a, b):     # xor two strings of different lengths
    """xor two strings of different length"""
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])


def parse(text):
    """Transforms the given ciphertext into ASCII whatchamacalit"""
    def hex2ascii(x):
        return chr(eval(x))
    word = []
    for i in xrange(len(text) - 2):
        word.append(text[i:i + 2])
    return "".join(map(hex2ascii, ['0x' + x for x in word]))

with open("ciphertexts") as fin:
    ciphertexts = fin.readlines()
    ciphertexts = [l.strip() for l in ciphertexts]
    parsed_ctexts = [parse(t) for t in ciphertexts]

# So now, if there is a space at the ith position in strxor(a,b) then a[i] and
# b[i] are separated by 32. I.e. if b[i] is alpha, then a[i] and b[i] are an
# uppercase/lowercase pair. If a derp derp
    digests = [(c, d, strxor(c, d))
               for c in parsed_ctexts for d in parsed_ctexts]
    deletions = []
    num_deletions = 0
    for s, k in enumerate(digests):
        if k[0] == k[1]:
            digests.remove(k)
#        for i in xrange(min(len(x), len(y))):
#            print "%s\t%s\t%s" % (x[0], y[0], z[0])


def candidates(text):
    """generates a list of cnadidates for each character of the text"""
    container = {}
    stuff = "\t\n " + string.uppercase + string.lowercase + string.punctuation
    for z, char in enumerate(text):
        for x in xrange(256):
            for y in xrange(256):
                if chr(x ^ y) == char and chr(x) in stuff and chr(y) in stuff:
                    container[z] = container.get(z, set([]))
                    container[z].update([chr(x), chr(y)])
                else:
                    pass
    return container

zahlen = "\t\n" + string.uppercase + string.lowercase + \
    string.punctuation + "0123456789"
if __name__ == '__main__':
    third = parsed_ctexts[2]
    fourth = parsed_ctexts[3]
    sixth = parsed_ctexts[5]
    d = [strxor(third, v) for v in parsed_ctexts]
    e = [strxor(fourth, v) for v in parsed_ctexts]
    f = [strxor(sixth, v) for v in parsed_ctexts]
    the_candidates1 = [candidates(elem)[:4] for elem in d]
    the_candidates2 = [candidates(elem)[:4] for elem in e]
    the_candidates3 = [candidates(elem)[:4] for elem in f]
    alpha = reduce(__and__, the_candidates1)
    beta = reduce(__and__, the_candidates2)
    gamma = reduce(__and__, the_candidates3)
