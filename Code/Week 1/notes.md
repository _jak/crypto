#Week one notes

##Problem 1
 Since all the ciphertexts are encrypted with the same key, by XORing the
 ciphertexts together, we get the plaintexts XORed together. I think the method
 we have to go for is:
      * Realize that if the output of this operation is a space " " then the
        plaintexts are probably the same characters, but uppercase and
        lowercase.
      * XOR the first plaintext with each other plaintext and find all the
        spaces in the output (or alphanumerics in the output -- they may be
        indicative of a space in one of the plaintexts)
      * make a list of candidates for each spot. (that is, find characters that
        could possibly xor to create that particular character)
      * create a list of candidates for each spot in each ciphertext. The
        intersection of all sets of candidates should yield at least one
        plaintext
      * xor this plaintext with its encrypted form to generate the key
      * use the key to decrypt message 11
